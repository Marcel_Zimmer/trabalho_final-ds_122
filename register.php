<?php
session_start();
require "db_functions.php";
$error = false;
$success = false;
$name = $email = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["confirm_password"])) {
    $conn = connect_db();
    $name = mysqli_real_escape_string($conn,$_POST["name"]);
    $email = mysqli_real_escape_string($conn,$_POST["email"]);
    $password = mysqli_real_escape_string($conn,$_POST["password"]);
    $confirm_password = mysqli_real_escape_string($conn,$_POST["confirm_password"]);

    if ($password == $confirm_password) {
      $password = md5($password);
      $sql = "INSERT INTO Users
      (name, email, password) VALUES
      ('$name', '$email', '$password');";
      if(mysqli_query($conn, $sql)){
        $acpt_msg="conta criada com sucesso";
        $_SESSION["acpt_msg"] = $acpt_msg;
        exit(header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/index.php"));
      }
      else {
        $error_msg = mysqli_error($conn);
      }
    }
    else {
      $error_msg = "Senha não confere com a confirmação.";
      $_SESSION["error_msg"] = $error_msg;
      header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/cadastro.php");
    }
  }
  else {
    $error_msg = "Por favor, preencha todos os dados.";
    $_SESSION["error_msg"] = $error_msg;
    header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/cadastro.php");
  }
}
?>
