<?php
require "db_functions.php";
require "authenticate.php";
if (!$login) {
  header("Location: index.php");
}?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Document</title>
</head>
<body>
  <?php
  require('db_credentials.php');

  $conn = new mysqli($servername, $username, $password, $dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }

  if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $code=$_POST["post"];
    $sql = "INSERT INTO comentario (codePost,comentario,idUsuario) values (".$_POST["post"].",'".$_POST["msg"]."',".$user_id.")";

    if ($conn->query($sql) === TRUE) {
      echo "New record created successfully";
    } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }

  ?>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">BLOGANDO</a>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item dropdown">
          </li>
        </ul>
      <div>
        <form action="logout.php" method="post">
          <button class="btn btn-primary" type="submite">Sair</button>
        </form>
      </div>
    </div>
  </div>
</nav>
<div id="cabecalho2">
  <?php
  if (isset($_GET['post'])){
    $code=$_GET['post'];
  }
  $sql="select * from posts where code=".$code;

  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
      $dataCriacao=date_create($row["dataCriacao"]);
      $dataAtualizacao=date_create($row["dataAtualizacao"]);
      $postagem = "<div class='posted'><p> ";
      $postagem = $postagem.$row["texto"]."</p>";
      $postagem = $postagem." <ul class='descricao'> <li>Categoria: ".$row["categoria"]."</li> <li> Data do Post:".date_format($dataCriacao,"d/m/Y H:i:s")."</li>";
      if (!(is_null($row["dataAtualizacao"]))){
        $postagem = $postagem."<li>Data da ult. Modificação: ".date_format($dataAtualizacao,"d/m/Y H:i:s")."</li>";
      }
      $postagem = $postagem. "</ul> </div>";
      echo $postagem;
    }
  }
  ?>
</div>
<form action="comentarios.php" method="post" name="formPost" id="formPost">
  <div id="msg_comentarios">
    <label for="msg">Seu Comentario:</label>
  </div>
  <div id="comentarios_label">
    <textarea name="msg" cols="100" rows="5"></textarea>
  </div>
  <div id="botao4">
    <?php
    echo "<input type='hiden' name='post' value='".$code."'>";
    ?>
  </div>
  <div id="botao5">
    <button id="botao3" type="submit" class="btn btn-primary" onclick="validar_coment()">Enviar</button>
  </div>
</form>
</form>
<form action="pagina_principal.php" method="post">
  <button id="botao_comentarios"class="btn btn-danger" type="submite">Sair</button>
</form>
<?php
$comentarios="<div> <h2>Comentarios </h2>";
$sql="select comentario.*,users.name from comentario,users where users.id=comentario.idUsuario and codePost=".$code." order by data";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    $dataComent=date_create($row["data"]);
    $comentarios = $comentarios."<div class='posted'><p>";
    $comentarios = $comentarios.$row["comentario"]."</p> <span>Usuario: ".$row["name"]."</span> <span> Data Comentario: ".date_format($dataComent,"d/m/Y H:i:s")."<span> </div>";
  }
  $comentarios = $comentarios."</div>";
  echo $comentarios;
}
?>
</body>
<script src="script/validation_coment.js"></script>
</html>
