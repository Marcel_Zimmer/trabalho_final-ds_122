<?php
require "register.php";
require "db_credentials.php"
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Cadastro</title>
  <script src="script_validation.js"></script>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand">BLOGANDO</a>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
        </ul>
      </div>
    </div>
  </nav>
  <h1 class="text-center"> cadastro de novo usuário</h1>
  <form class="text-center" form action="register.php" method="post">
    <div class="row" id="tabela">
      <label id="texto"class="form-label">Nome de usuário</label>
      <input type="name" id="name" name="name" required>
    </div>
    <div  class="row" id="tabela">
      <label id="texto"class="form-label">E-mail</label>
      <input type="email" id="email" name="email" required>
    </div>
  </div>
  <div class="row" id="tabela">
    <label id="texto"class="form-label"value="">Senha</label>
    <input type="password" id="password" name="password" required>
  </div>
  <div class="row" id="tabela">
    <label id="texto" class="form-label"value="">Confirme a Senha</label>
    <input type="password" id="confirm_password" name="confirm_password" required>
  </div>
  <div class="col-12">
    <button type="submit" class="btn btn-primary" href="register.php">Criar Conta</button>
    <?php
    $error_msg = isset($_SESSION["error_msg"]) ? $_SESSION["error_msg"] : "";
    unset($_SESSION["error_msg"]);
    if ($error_msg  != '') { ?>
      <div class="alert alert-danger" role="alert">
        <?php echo $error_msg; ?>
      </div>
    <?php }?>
  </div>
</form>
<form action="index.php" method="post">
  <button id="botao8" type="submit" class="btn btn-danger">Voltar</button>
</form>
<link rel="stylesheet" href="script_validation.js">
</body>
</html>
