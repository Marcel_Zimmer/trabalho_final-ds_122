<?php
require 'db_credentials.php';
$conn = mysqli_connect($servername, $username, $password);
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

$sql = "CREATE DATABASE $dbname";
if (mysqli_query($conn, $sql)) {
  echo "<br>Database created successfully<br>";
} else {
  echo "<br>Error creating database: " . mysqli_error($conn);
}

$sql = "USE $dbname";
if (mysqli_query($conn, $sql)) {
  echo "<br>Database changed successfully<br>";
} else {
  echo "<br>Error changing database: " . mysqli_error($conn);
}

$sql = "CREATE TABLE Users (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100) NOT NULL,
  email VARCHAR(100) NOT NULL,
  password VARCHAR(128) NOT NULL,
  created_at DATETIME,
  updated_at DATETIME,
  last_login_at DATETIME,
  last_logout_at DATETIME,
  UNIQUE (email)
)";

if (mysqli_query($conn, $sql)) {
  echo "<br>Table created successfully<br>";
} else {
  echo "<br>Error creating database: " . mysqli_error($conn);
}

$sql = "CREATE TABLE Posts (
  code INT NOT NULL AUTO_INCREMENT ,
  dataCriacao DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  dataAtualizacao DATETIME NULL DEFAULT NULL ,
  categoria VARCHAR(50) NOT NULL ,
  texto VARCHAR(1000) NOT NULL ,
  idUsuario INT,
  PRIMARY KEY (code))";

  if (mysqli_query($conn, $sql)) {
    echo "<br>Table created successfully<br>";
  } else {
    echo "<br>Error creating database: " . mysqli_error($conn);
  }

  $sql = "CREATE TABLE comentario (
    code INT NOT NULL AUTO_INCREMENT,
    codePost INT,
    data DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    comentario VARCHAR(1000) NOT NULL ,
    idUsuario INT,
    PRIMARY KEY (code))";

    if (mysqli_query($conn, $sql)) {
      echo "<br>Table created successfully<br>";
    } else {
      echo "<br>Error creating database: " . mysqli_error($conn);
    }

    mysqli_close($conn)
    ?>
