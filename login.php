<?php
require "db_functions.php";
require "authenticate.php";

$error = false;
$login_password = $email = "";
if (!$login && $_SERVER["REQUEST_METHOD"] == "POST") {
  if (isset($_POST["email"]) && isset($_POST["password"])) {
    $conn = connect_db();
    $email = mysqli_real_escape_string($conn,$_POST["email"]);
    $login_password = mysqli_real_escape_string($conn,$_POST["password"]);
    $login_password = md5($login_password);
    $sql = "SELECT id,name,email,password FROM Users
    WHERE email = '$email';";
    $result = mysqli_query($conn, $sql);
    if($result){
      if (mysqli_num_rows($result) > 0) {
        $user = mysqli_fetch_assoc($result);
        if ($user["password"] == $login_password) {
          $_SESSION["user_id"] = $user["id"];
          $_SESSION["user_name"] = $user["name"];
          $_SESSION["user_email"] = $user["email"];
          header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/pagina_principal.php");
          exit();
        }
        else {
          $error_msg = "Senha incorreta!";
          $_SESSION["error_msg"] = $error_msg;
        }
      }
      else{
        $error_msg = "Usuário não encontrado!";
        $_SESSION["error_msg"] = $error_msg;
      }
    }
    else {
      $error_msg = mysqli_error($conn);
      $_SESSION["error_msg"] = $error_msg;;
    }
  }
  else {
    $error_msg = "Por favor, preencha todos os dados.";
    $_SESSION["error_msg"] = $error_msg;
  }
}
header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/index.php");
?>
