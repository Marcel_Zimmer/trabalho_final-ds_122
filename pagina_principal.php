<?php
require "db_functions.php";
require "authenticate.php";
if (!$login) {
  header("Location: index.php");
}?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Pagina Principal</title>
  <?php
  require('db_credentials.php');
  $conn = new mysqli($servername, $username, $password, $dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }
  ?>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
    <div class="container-fluid ">
      <a class="navbar-brand" href="#">BLOGANDO</a>
      <div class="collapse navbar-collapse">
        <ul class="navbar -nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
        </ul>
        <form id="pesquisar">
          <input name="search" placeholder='Pesquisar por Categoria'/>
          <button class="btn btn-primary" type="submite">Pesquisar</button>
        </form>
        <form action="post.php" >
          <div id="postagem">
            <button class="btn btn-primary" type="submite">Novo Post</button>
          </div>
        </form>
        <div>
          <form action="logout.php" method="post">
            <button class="btn btn-primary" type="submite">Sair</button>
          </form>
        </div>
      </div>
    </div>
  </nav>
  <div id="novo_post">
    <?php
    $voltar = false;
    if (isset($_GET['search'])){
      $sql = "select * from posts where categoria like '%".$_GET["search"]."' order by dataCriacao desc";
      $voltar = true;
    } else {
      $sql = "select posts.*,users.name from posts,users where (posts.idUsuario=users.id) order by dataCriacao desc";
    }
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $dataCriacao=date_create($row["dataCriacao"]);
        $dataAtualizacao=date_create($row["dataAtualizacao"]);
        $postagem = "<div class='posted'><p> ";
        $postagem = $postagem.$row["texto"]."</p>";
        $postagem = $postagem." <ul class='descricao'> <li>Usuario: ".$row["name"]. "<li>Categoria: ".$row["categoria"]."</li> <li> Data do Post: ".date_format($dataCriacao,"d/m/Y H:i:s")."</li>";
        if (!(is_null($row["dataAtualizacao"]))){
          $postagem = $postagem." <li>Data da ult. Modificação: ".date_format($dataAtualizacao,"d/m/Y H:i:s")."</li>";
        }
        $postagem = $postagem."<li><a href='edit.php?post=".$row["code"]."'> editar </a></li>";
        $postagem = $postagem."</ul><br><div class='comentario'>";
        $postagem = $postagem."<form method='get' action='comentarios.php?post=".$row["code"]."'> <input type='hidden' name='post' value='".$row["code"]."'><input type='submit'  class='btn btn-primary' value='Comentarios'></form>";
        $postagem = $postagem."</div></div>";
        echo $postagem;
      }
    }
    if ($voltar):?>
    <form action="pagina_principal.php" method="post">
      <button id="botao7" class="btn btn-primary" type="submite">Voltar</button>
    </form>
  <?php endif ?>
</form>
</div>
</body>
</html>
