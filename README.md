Tecnologia em Análise e Desenvolvimento de Sistemas
Setor de Educação Profissional e Tecnológica - SEPT
Universidade Federal do Paraná - UFPR
---
DS122 - Desenvolvimento de Aplicações Web 1
Prof. Alexander Robert Kutzke
---
Alunos: Cristiano Martins de Souza, Marcel Alessandro Zimmer, Matheus Dionysio Classe e Victoria Kunz Vieira.

**Trabalho Final de Web**

---

# APLICAÇÃO WEB *BLOGANDO*

Essa é uma aplicação de um blog simples, onde um usuário pode se cadastrar para então:
* Criar um post;
* Editar um post;
* Comentar em posts do site;
 
 A aplicação inclui a implementação de front-end, back-end e também possui integração com um banco de dados.
 Para rodar o código, primeiro, é preciso preencher as credenciais no arquivo **db_credendials** e acessar a página **create_db_tables**, quando executado, ela criará todas as tabelas e as funções necessárias para o funcionamento correto do banco de dados. O banco contém 3 tabelas, a tabela de posts, onde é armazenado as informações das postagens, a tabela users, onde é armazenado os dados dos usuários e a tabela de comentario, onde é armazenado os dados dos comentários feitos.

 ---
## Utilizando a aplicação
A página inicial da aplicação apresenta as informações sobre o site e um campo para o usuário fazer seu login ou seu cadastro, caso ainda não seja cadastrado. Uma vez que o usuário realizar o login a página inicial ira redirecioná-lo para a página onde se encontram as postagens.
### Login
* O usuário insere seu nome de usuário (verifica se o nome de usuário está cadastrado no banco de dados);
* O usuário insere a senha (verfica se a senha corresponde ao nome de usuário cadastrado)
### Cadastro
* O usuário cria um nome de usuário;
* O usuário insere o email de cadastro (há a validação para verificar se o caractere **@** está presente no dado informado);
* O usuário digita uma senha;
* O usuário digita a senha novamente (há a validação para verificar se as duas senhas inseridas são iguais);
### Postagem
Após entrar em sua conta, o usuário pode fazer novos posts e visualizar os posts existentes de diferentes usuários (que informam a data do post, a data da última edição, o autor do post, e os comentários do post).
### Comentarios
Ao clicar no botão de comentário, o usuário vai ser redirecionado para a página de comentários. La é possivel visualizar os comentários feitos na postagem e inserir novos comentários.
### Editar
O usuário pode editar postagens existentes, alterando o texto delas e a categoria.


