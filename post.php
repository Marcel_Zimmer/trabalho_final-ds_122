<?php
require "db_functions.php";
require "authenticate.php";
if (!$login) {
  header("Location: index.php");
}?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Fazer Postagem xD</title>
  <?php
  require('db_credentials.php');
  $conn = new mysqli($servername, $username, $password, $dbname);
  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }
  if ($_SERVER["REQUEST_METHOD"] == "POST"){
    echo $user_id;
    $sql = "INSERT INTO posts (texto,categoria,idUsuario) values ('".$_POST["msg"]."','".$_POST["ctg"]."',".$user_id.")";
    if ($conn->query($sql) === TRUE) {
      header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/pagina_principal.php");
    } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }
  ?>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">BLOGANDO</a>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item dropdown">
          </li>
        </ul>
      <div>
        <form action="logout.php" method="post">
          <button class="btn btn-primary" type="submite">Sair</button>
        </form>
      </div>
    </div>
  </nav>
  <div id="novo_p">
    <form action="post.php" method="post" name="formPost" id="formPost">
      <div id="labe">
        <label  for="msg">Seu Texto:</label>
      </div>
      <textarea name="msg" id="" cols="50" rows="10"></textarea>
      <div id="area">
        <label for="ctg">Categoria:</label>
      </div>
      <div id="labee">
        <textarea name="ctg"></textarea>
      </div>
      <button id="botao" type="submit" class="botao btn btn-primary" onclick="validar()">Enviar</button>
    </form>
    <form action="pagina_principal.php" method="post">
      <button id="botao" type="submit" class="botao btn btn-danger" onclick="validar()">cancelar</button>
    </form>
</body>
<script src="script/script_validation.js"></script>
</html>
