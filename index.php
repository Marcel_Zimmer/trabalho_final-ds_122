<?php
require "db_functions.php";
require "authenticate.php";
if ($login) {
  header("Location:pagina_principal.php");
}?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Document</title>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">BLOGANDO</a>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item dropdown">
          </li>
        </ul>
        <form action="login.php" method="post">
          <label class="navbar-brand" for="email">E-mail:</label>
          <input type="email" id="email" name="email" required>
          <label class="navbar-brand" for="password">Senha:</label>
          <input type="password" id="password" name="password" required>
          <button type="submit" class="btn btn-primary" href="register.php">Entrar</button>
        </form>
        <a id="registro_index" href="Cadastro.php">registre-se</a>
      </div>
    </div>
  </nav>
  <div id="msg_register">
    <?php
    $error_msg = isset($_SESSION["error_msg"]) ? $_SESSION["error_msg"] : "";
    unset($_SESSION["error_msg"]);
    $acpt_msg = isset($_SESSION["acpt_msg"]) ? $_SESSION["acpt_msg"] : "";
    unset($_SESSION["acpt_msg"]);
    if ($error_msg != '') : ?>
    <div class="alert alert-danger no-box-shadow" role="alert ">
      <?php echo $error_msg; ?>
    </div>
  <?php elseif ($acpt_msg != ''): ?>
    <div class="alert alert-success no-box-shadow" role="alert">
      <?php echo $acpt_msg; ?>
    </div>
  <?php endif ?>
</div>
<div class="jumbotron jumbotron-fluid intro" id="nav">
  <div class="container">
    <h1 class="display-4">BLOGANDO</h1>
    <p class="lead">
      <p>Lorem ipsum dolor sit amet. Eos minus dolorem qui fuga accusantium non voluptatum tempore! Ab quod nemo sit tempora vero qui vitae velit et deleniti inventore aut voluptas odio. </p><p>Qui dolorem voluptas et impedit veniam cum atque nisi. Qui magnam quasi aut aperiam quia aut aspernatur voluptatem est Quis totam. Qui temporibus voluptate aut temporibus natus in consequuntur doloribus. </p><p>Aut eius commodi eum deleniti quia hic nostrum doloremque qui libero optio est commodi inventore aut fuga nihil. Aut sint inventore et repellendus quasi aut dolor iure At quis repudiandae in nostrum quasi est galisum earum. Ut delectus ipsum qui molestiae distinctio et officiis quia et nihil itaque et autem galisum! 33 itaque enim ut nemo explicabo hic quia exercitationem. </p><p>33 eius veniam eos magnam repellat est dignissimos rerum non internos perspiciatis. Eum totam assumenda et voluptas quia est nemo iusto aut sint necessitatibus aut nihil quod et doloribus soluta. </p><p>Non rerum perferendis nam ducimus voluptatem et veniam deleniti ex blanditiis adipisci est assumenda voluptatem rem ullam magnam aut veritatis iure. Quo itaque sequi cum dolores voluptates qui reiciendis corrupti non repellat dolorum eum sunt molestiae id nostrum omnis. Ad esse modi in reiciendis totam ea neque omnis quo explicabo debitis id saepe eveniet eos mollitia velit et culpa veritatis. </p><p>Ut quia officiis non iusto eveniet id fuga fugit? Et beatae quidem aut voluptates harum in rerum optio aut blanditiis eius sed voluptatum sunt sit molestiae sapiente qui perferendis nulla. At molestias labore in nostrum consectetur et sapiente dolorum. Est officia officiis qui optio laudantium ex aliquam esse sed voluptates blanditiis vel obcaecati eius. </p><p>Ad numquam sint At ipsam dolorum in aliquam eaque id reprehenderit voluptatem. A distinctio voluptatem et necessitatibus illo vel dolorem voluptatum ea dolorem quam? Aut culpa nobis est iusto minima ex distinctio corrupti! Est cumque beatae qui voluptates laboriosam At maiores beatae qui maiores omnis.<p>Lorem ipsum dolor sit amet. Aut consequatur numquam eum blanditiis sint sed quia unde et deserunt maxime et optio sunt. Qui similique nulla non praesentium architecto qui quia Quis ea cumque alias qui iusto perspiciatis. Qui enim deleniti vel esse exercitationem et odit perspiciatis qui fugiat praesentium ut dolorem obcaecati? </p><p>Ea sunt magnam qui blanditiis sapiente sit eius tempore rem dicta exercitationem hic officia consequatur. Id delectus provident qui laborum magnam id quasi galisum eum consequuntur nihil aut voluptas illo At numquam omnis? Est aperiam cumque et excepturi iure qui enim nihil est optio officia est officia distinctio. </p><p>Eum temporibus quasi ut consectetur aliquid ut dolorum odio aut molestiae facilis ab officiis magnam. Et ratione galisum qui nulla libero qui accusamus galisum in sapiente corrupti. Ut quia fugit hic molestiae iusto et autem minima est galisum quae ea excepturi pariatur vel exercitationem sint et doloremque illo. </p>
    </p>
  </div>
</div>
<div class="container maiin "id="corpa">
  <h1 class="text-center">Sobre nos</h1>
  <p>Lorem ipsum dolor sit amet. Aut dolorem galisum qui aliquam rerum et perferendis enim et alias sint sed sequi reprehenderit non odio saepe. Est omnis vero est magnam quidem eos nihil perferendis ex accusantium quia est nulla fugit ab eligendi quis. Ut ipsam natus qui corporis sint eos facere expedita id esse ratione ut eaque eius. </p><p>Est doloremque consequatur hic quam internos et accusamus amet in eius iste aut error nisi ut culpa autem quo molestiae aspernatur. Eum reiciendis corporis ea repellendus deserunt est quam distinctio. Quo culpa molestias aut quibusdam voluptas aut iure quam cum corporis magni et quia enim et omnis laboriosam. Aut inventore reprehenderit non saepe Quis sed nobis dolorem id consequatur cupiditate ut quisquam exercitationem. </p>
</div>
</body>
</html>
