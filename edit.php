<?php
require "db_functions.php";
require "authenticate.php";
if (!$login) {
  header("Location: index.php");
}?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <title>Document</title>
  <?php
  require('db_credentials.php');

  $conn = new mysqli($servername, $username, $password, $dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $agora = date('Y-m-d H:i:s');
    $sql = "UPDATE posts SET texto=?, categoria=?, dataAtualizacao=? WHERE code=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssi", $_POST["msg"], $_POST["ctg"], $agora, $_POST["code"]);
    if ($stmt->execute()) {
      echo "Record updated successfully";
      header("Location: " . dirname($_SERVER['SCRIPT_NAME']) . "/pagina_principal.php");
    } else {
      echo "Error updating record: " . $stmt->error;
    }
  }
  elseif ($_SERVER["REQUEST_METHOD"] == "GET"){
    $code=$_GET["post"];
  }
  ?>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">BLOGANDO</a>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
          <li class="nav-item dropdown">
          </li>
        </ul>
      <div>
        <form action="logout.php" method="post">
          <button class="btn btn-primary" type="submite">Sair</button>
        </form>
      </div>
    </div>
  </div>
</nav>
<form action="edit.php" method="post" name="formPost" id="formPost">
  <div id="cabecalho">
    <h2>Escreva aqui sua postagem: </h2>
    <textarea name="msg" id="" cols="80" rows="10"></textarea>
  </div>
  <div id="cabecalho">
    <label for="ctg">Categoria:</label>
  </div>
  <div id="cabecalho">
    <textarea name="ctg" ></textarea>
  </div>
  <div id="cabecalho">
    <button type="submit" class="botao btn btn-primary" onclick="validar()">Editar</button>
    <?php
    echo "<input type='hidden' name='code' value='".$code."' >";
    ?>
  </form>
</div>
</form>
<form id="botao6" action="pagina_principal.php" method="post">
  <button type="submit" class="botao btn btn-danger" onclick="validar()">cancelar</button>
</form>
<?php
$stmt = $conn->prepare("SELECT * FROM posts WHERE code = ?");
$stmt->bind_param("i", $_GET["post"]);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows > 0) {
  while($row = $result->fetch_assoc()) {
    echo "<script> var texto = formPost.msg;  var categoria = formPost.ctg; categoria.value=".$row['categoria']."; texto.value='".$row["texto"]."'</script>";
  }
}
?>
</body>
<script src="script/script_validation.js"></script>
</html>
